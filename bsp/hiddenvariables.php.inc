<?php
//Ü
	// ####################################################
	// MySQL
	// ####################################################

	$database_host='localhost';
	$database_username='telegram';
	$database_userpass='jourpass';

	$database_name='telegram';

	// Der Name der User-Datenbank, für jeden Bot eine extra Datenbank
	$database_table_main="$database_name.FStrimstatbot";

	function connectpdo() { // Liefert eine pdo instanz
	    global
		$database_host,$database_username,$database_userpass;
	    $pdo=new PDO('mysql:host='.$database_host, $database_username, $database_userpass,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8MB4\''));
	    if($pdo->errorInfo()[0]<>'00000') error_log("$botname ".print_r($pdo->errorInfo(),true));
	return $pdo;
	}


	// ####################################################
	// BOT
	// ####################################################
	
	// Daten vom @botfather
	$bot_name="botname";
	$bot_benutzername="botnamebot";
	$bot_token="yourtoken";

	// Die Domain des Servers
	$bot_server="server.domain";
	// Nur eine Vereinfachung und Gewohnheit
	$bot_db=$database_table_main;
	// Bot verantwortlicher // Nutzung beim WEB-Aufruf Telegram-Bot-Test
	$bot_owner=123456789; // At the moment not used
	// Server verantwortlicher // noch keine Nutzung, wird für die Sprachen die Rechte vergeben, da diese für alle Bots die gleichen sind
	$bot_server_owner=123456789; // At the moment not used
	// Bot Admins // Setzen der Nutzerrechte (mod, user)
	$bot_admins[12345678]='infotext';
	// Kann via Browser abgerufen werden (geheimes Verzeichnis...)webhook.php?info
	$bot_info="FS-Bot Trim Stat Bot";
?>