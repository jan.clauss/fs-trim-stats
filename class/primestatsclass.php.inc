<?php

/**** Klasse zum einlesen der Prime-Stats aus Telegram und Tab-Separiert
*
*	@Author: Jan Clauß
* @Version: 1.6 - 2023-07-01
*
* 1.2   Komplett überarbeitet, jetzt ist (sollte) die Reihenfolge der Stats egal sein.
* 1.201 originalposition in statxt
*       generierten TabSeparierteStats
* 1.21  Filter für createTSV
* 1.30  error, wenn nicht ALL TIME
*       Matryoshka Links Created ergänz
* 1.31  Superposition ergänzt
*       Konstanten für die Indexe von Agentname, Faction, Date und Time
*
* 1.32  Zeitzone und Timestamp in den Stats
*
* 1.4   Funktion getNewOriginalposNumbers() zum neu Vergeben der 'originalpos', Option $checkoriginalpos zum Prüfen der 'originalpos'
*
* 1.5   Statsgruppen ergänzt
*
* 1.6   Prüfen der Anzahl der Stats-Werte
*
*/

class primestats
{

	const IDX_AGENTNAME = 1;
	const IDX_FACTION = 2;
	const IDX_DATE = 3;
	const IDX_TIME = 4;

	public $minimumstatsnumber = 12; // Mindestanzahl an Stats-Werten (inclusive Timespan, Agenname...). Sonst wird eine Warnung mit aus gegeben
		// Beim FS in Graz am 3.8.2024 gab es das Problem, das in den Stats nur Level und beide AP-Werte vorhanden waren
	public $checkoriginalpos = true; // Prüft beim Auswerten der Stats, ob die Position den Werten in $stattxt['originalpos'] entspricht,
	                                  // gibt eine Meldung ins Fehlerlog, wenn originalposition in Array nicht stimmt

	public $statgroups =array(
		'---' => array('idx' => '---', 'name' => 'Head'),
		'DIS' => array('idx' => 'DIS', 'name' => 'Discovery'),
		'BUI' => array('idx' => 'BUI', 'name' => 'Building'),
		'RES' => array('idx' => 'RES', 'name' => 'Resource Gathering'),
		'STR' => array('idx' => 'STR', 'name' => 'Streaks'),
		'COM' => array('idx' => 'COM', 'name' => 'Combat'),
		'DEF' => array('idx' => 'DEF', 'name' => 'Defense'),
		'HEA' => array('idx' => 'HEA', 'name' => 'Health'),
		'MIS' => array('idx' => 'MIS', 'name' => 'Missions'),
		'BOU' => array('idx' => 'BOU', 'name' => 'Bounties'),
		'EVE' => array('idx' => 'EVE', 'name' => 'Events'),
		'REC' => array('idx' => 'REC', 'name' => 'Recursion'),
		'MEN' => array('idx' => 'MEN', 'name' => 'Mentoring'),
		'SUB' => array('idx' => 'SUB', 'name' => 'Subscription'),
	);

	public $stattxt = array(
		array('idx' =>    0, 'originalpos' =>   10, 'group' => '---', 'typ' => 'A', 'txt' => 'Time Span'),
		array('idx' =>    1, 'originalpos' =>   20, 'group' => '---', 'typ' => 'A', 'txt' => 'Agent Name'),
		array('idx' =>    2, 'originalpos' =>   30, 'group' => '---', 'typ' => 'A', 'txt' => 'Agent Faction'),
		array('idx' =>    3, 'originalpos' =>   40, 'group' => '---', 'typ' => 'A', 'txt' => 'Date (yyyy-mm-dd)'),
		array('idx' =>    4, 'originalpos' =>   50, 'group' => '---', 'typ' => 'A', 'txt' => 'Time (hh:mm:ss)'),
		array('idx' =>    5, 'originalpos' =>   60, 'group' => '---', 'typ' => 'N', 'txt' => 'Level'),
		array('idx' =>    6, 'originalpos' =>   70, 'group' => '---', 'typ' => 'N', 'txt' => 'Lifetime AP'),
		array('idx' =>    7, 'originalpos' =>   80, 'group' => '---', 'typ' => 'N', 'txt' => 'Current AP'),
		array('idx' =>    8, 'originalpos' =>   90, 'group' => 'DIS', 'typ' => 'N', 'txt' => 'Unique Portals Visited', 'badges' => array('name' => 'Explorer', 100, 1000, 2000, 10000, 30000)),
		array('idx' =>    9, 'originalpos' =>  120, 'group' => 'DIS', 'typ' => 'N', 'txt' => 'Portals Discovered'),
		array('idx' =>   10, 'originalpos' =>  130, 'group' => 'DIS', 'typ' => 'N', 'txt' => 'Seer Points', 'badges' => array('name' => 'Seer', 10, 50, 200, 500, 5000)),
		array('idx' =>   11, 'originalpos' =>  140, 'group' => 'DIS', 'typ' => 'N', 'txt' => 'XM Collected'),
		array('idx' =>   12, 'originalpos' =>  150, 'group' => 'DIS', 'typ' => 'N', 'txt' => 'OPR Agreements', 'badges' => array('name' => 'Recon', 100, 750, 2500, 5000, 10000)),
		array('idx' =>   13, 'originalpos' =>  500, 'group' => 'HEA', 'typ' => 'N', 'txt' => 'Distance Walked', 'badges' => array('name' => 'Trekker', 10, 100, 300, 1000, 2500)),
		array('idx' =>   14, 'originalpos' =>  180, 'group' => 'BUI', 'typ' => 'N', 'txt' => 'Resonators Deployed', 'badges' => array('name' => 'Builder', 2000, 10000, 30000, 100000, 200000)),
		array('idx' =>   15, 'originalpos' =>  190, 'group' => 'BUI', 'typ' => 'N', 'txt' => 'Links Created', 'badges' => array('name' => 'Connector', 50, 1000, 5000, 25000, 100000)),
		array('idx' =>   16, 'originalpos' =>  200, 'group' => 'BUI', 'typ' => 'N', 'txt' => 'Control Fields Created', 'badges' => array('name' => 'Mind Controller', 100, 500, 2000, 10000, 40000)),
		array('idx' =>   17, 'originalpos' =>  210, 'group' => 'BUI', 'typ' => 'N', 'txt' => 'Mind Units Captured', 'badges' => array('name' => 'Illuminator', 5000, 50000, 250000, 1000000, 4000000)),
		array('idx' =>   18, 'originalpos' =>  220, 'group' => 'BUI', 'typ' => 'N', 'txt' => 'Longest Link Ever Created'),
		array('idx' =>   19, 'originalpos' =>  230, 'group' => 'BUI', 'typ' => 'N', 'txt' => 'Largest Control Field'),
		array('idx' =>   20, 'originalpos' =>  240, 'group' => 'BUI', 'typ' => 'N', 'txt' => 'XM Recharged', 'badges' => array('name' => 'Recharcher', 1000000, 10000000, 30000000, 100000000, 250000000)),
		array('idx' =>   21, 'originalpos' =>  250, 'group' => 'BUI', 'typ' => 'N', 'txt' => 'Portals Captured', 'badges' => array('name' => 'Liberator', 100, 1000, 5000, 15000, 40000)),
		array('idx' =>   22, 'originalpos' =>  260, 'group' => 'BUI', 'typ' => 'N', 'txt' => 'Unique Portals Captured', 'badges' => array('name' => 'Pioneer', 20, 200, 1000, 5000, 20000)),
		array('idx' =>   23, 'originalpos' =>  270, 'group' => 'BUI', 'typ' => 'N', 'txt' => 'Mods Deployed', 'badges' => array('name' => 'Ingeneer', 150, 1500, 5000, 20000, 50000)),
		array('idx' =>   24, 'originalpos' =>  340, 'group' => 'COM', 'typ' => 'N', 'txt' => 'Resonators Destroyed', 'badges' => array('name' => 'Purifier', 2000, 10000, 30000, 100000, 300000)),
		array('idx' =>   25, 'originalpos' =>  350, 'group' => 'COM', 'typ' => 'N', 'txt' => 'Portals Neutralized'),
		array('idx' =>   26, 'originalpos' =>  360, 'group' => 'COM', 'typ' => 'N', 'txt' => 'Enemy Links Destroyed'),
		array('idx' =>   27, 'originalpos' =>  370, 'group' => 'COM', 'typ' => 'N', 'txt' => 'Enemy Fields Destroyed'),
		array('idx' =>   28, 'originalpos' =>  440, 'group' => 'DEF', 'typ' => 'N', 'txt' => 'Max Time Portal Held', 'badges' => array('name' => 'Guardian', 3, 10, 20, 90, 150)),
		array('idx' =>   29, 'originalpos' =>  450, 'group' => 'DEF', 'typ' => 'N', 'txt' => 'Max Time Link Maintained'),
		array('idx' =>   30, 'originalpos' =>  460, 'group' => 'DEF', 'typ' => 'N', 'txt' => 'Max Link Length x Days'),
		array('idx' =>   31, 'originalpos' =>  470, 'group' => 'DEF', 'typ' => 'N', 'txt' => 'Max Time Field Held'),
		array('idx' =>   32, 'originalpos' =>  480, 'group' => 'DEF', 'typ' => 'N', 'txt' => 'Largest Field MUs x Days'),
		array('idx' =>   33, 'originalpos' =>  520, 'group' => 'MIS', 'typ' => 'N', 'txt' => 'Unique Missions Completed', 'badges' => array('name' => 'SpecOps', 5, 25, 100, 200, 500)),
		array('idx' =>   34, 'originalpos' =>  280, 'group' => 'RES', 'typ' => 'N', 'txt' => 'Hacks', 'badges' => array('name' => 'Hacker', 2000, 10000, 30000, 100000, 200000)),
		array('idx' =>   35, 'originalpos' =>  300, 'group' => 'RES', 'typ' => 'N', 'txt' => 'Glyph Hack Points', 'badges' => array('name' => 'Translator', 200, 2000, 6000, 20000, 50000)),
		array('idx' =>   36, 'originalpos' =>  330, 'group' => 'STR', 'typ' => 'N', 'txt' => 'Longest Sojourner Streak', 'badges' => array('name' => 'Sojourner', 15, 30, 60, 180, 360), 'comments' => '2021-04-02 vorher Longest Hacking Streak'),
		array('idx' =>   37, 'originalpos' =>  930, 'group' => 'MEN', 'typ' => 'N', 'txt' => 'Agents Recruited', 'badges' => array('name' => 'Recruiter', 2, 10, 25, 50, 100), 'comments' => 'vorher Agents Successfully Recruited'),
		array('idx' =>   38, 'originalpos' =>  550, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Mission Day(s) Attended', 'badges' => array('name' => 'Mission Day', 1, 3, 6, 10, 20)),
		array('idx' =>   39, 'originalpos' =>  560, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'NL-1331 Meetup(s) Attended', 'badges' => array('name' => 'NL-1331 Meetups', 1, 5, 10, 25, 50)),
		array('idx' =>   40, 'originalpos' =>  570, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'First Saturday Events', 'badges' => array('name' => 'First Saturday', 1, 6, 12, 24, 36)),
		array('idx' =>   41, 'originalpos' =>  590, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Clear Fields Events', 'badges' => array('name' => 'OCF', 1, 3, 6, 10, 20)),
		array('idx' =>   42, 'originalpos' =>  600, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'OPR Live Events', 'badges' => array('name' => 'OPR-Live', 1, 3, 6, 10, 20)),
		array('idx' =>   43, 'originalpos' =>  610, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Prime Challenges', 'badges' => array('name' => 'Prime Challenges', 1, 2, 3, 4)),
		array('idx' =>   44, 'originalpos' =>  630, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Stealth Ops Missions', 'badges' => array('name' => 'Stealt Ops', 1, 3, 6, 10, 20)),
		array('idx' =>   45, 'originalpos' =>  940, 'group' => 'REC', 'typ' => 'N', 'txt' => 'Recursions'),
		array('idx' =>   46, 'originalpos' =>  160, 'group' => 'DIS', 'typ' => 'N', 'txt' => 'Portal Scans Uploaded', 'badges' => array('name' => 'Scout', 50, 250, 1000, 3000, 6000)),
		array('idx' =>   47, 'originalpos' =>  100, 'group' => 'DIS', 'typ' => 'N', 'txt' => 'Unique Portals Drone Visited', 'comments' => '20200605'),
		array('idx' =>   48, 'originalpos' =>  110, 'group' => 'DIS', 'typ' => 'N', 'txt' => 'Furthest Drone Distance', 'comments' => '20200817 vorher Furthest Drone Flight Distance'),
		array('idx' =>   49, 'originalpos' =>  290, 'group' => 'RES', 'typ' => 'N', 'txt' => 'Drone Hacks', 'badges' => array('name' => 'Maverick', 250, 1000, 2000, 5000, 10000), 'comments' => '20200605'),
		array('idx' =>   50, 'originalpos' =>  490, 'group' => 'DEF', 'typ' => 'N', 'txt' => 'Forced Drone Recalls', 'comments' => '20200609'),
		array('idx' =>   51, 'originalpos' =>  170, 'group' => 'DIS', 'typ' => 'N', 'txt' => 'Uniques Scout Controlled', 'badges' => array('name' => 'Recon', 100, 500, 1000, 5000, 12000), 'comments' => '20200817 vorher Scout Controller on Unique Portals'),
		array('idx' =>   52, 'originalpos' =>  380, 'group' => 'COM', 'typ' => 'N', 'txt' => 'Battle Beacon Combatant', 'comments' => '20201002'),
		array('idx' =>   53, 'originalpos' =>  510, 'group' => 'HEA', 'typ' => 'N', 'txt' => 'Kinetic Capsules Completed', 'comments' => '2020-10-19'),
		array('idx' =>   54, 'originalpos' =>  950, 'group' => 'SUB', 'typ' => 'N', 'txt' => 'Months Subscribed'),
		array('idx' =>   55, 'originalpos' =>  650, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Kinetic Capsules Completed', 'alt' => 'Event Kinetic Capsules Completed', 'comments' => '2021-03-19 Doppelter Eintrag für Event, extra Zähler für den Zeitraum'),
		array('idx' =>   56, 'originalpos' =>  620, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Intel Ops Missions', 'badges' => array('name' => 'Intel Ops', 1, 3, 6, 10, 20)),
		array('idx' =>   57, 'originalpos' =>  320, 'group' => 'STR', 'typ' => 'N', 'txt' => 'Completed Hackstreaks', 'badges' => array('name' => 'Epoch', 2, 4, 8, 30, 60)),
		array('idx' =>   58, 'originalpos' =>  390, 'group' => 'COM', 'typ' => 'N', 'txt' => 'Drones Returned'),
		array('idx' =>   59, 'originalpos' =>  580, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Second Sunday Events', 'badges' => array('name' => 'Second Sunday', 1, 6, 12, 24, 36)),
		array('idx' =>   60, 'originalpos' =>  400, 'group' => 'COM', 'typ' => 'N', 'txt' => 'Machina Links Destroyed'),
		array('idx' =>   61, 'originalpos' =>  410, 'group' => 'COM', 'typ' => 'N', 'txt' => 'Machina Resonators Destroyed'),
		array('idx' =>   62, 'originalpos' =>  420, 'group' => 'COM', 'typ' => 'N', 'txt' => 'Machina Portals Neutralized'),
		array('idx' =>   63, 'originalpos' =>  640, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Urban Ops Missions', 'comments' => 'badges => array(name => Stealt Ops, 1, 3, 6, 10, 20)'),
		array('idx' =>   64, 'originalpos' =>  430, 'group' => 'COM', 'typ' => 'N', 'txt' => 'Machina Portals Reclaimed', 'badges' => array('name' => 'Reclaimer', 100, 1000, 5000, 15000, 40000), 'comments' => '20230522'),
		array('idx' =>   65, 'originalpos' =>  310, 'group' => 'RES', 'typ' => 'N', 'txt' => 'Overclock Hack Points', 'comments' => '20231106'),
		array('idx' =>   66, 'originalpos' =>  530, 'group' => 'BOU', 'typ' => 'N', 'txt' => 'Research Bounties Completed', 'comments' => '20240208'),
		array('idx' =>   67, 'originalpos' =>  540, 'group' => 'BOU', 'typ' => 'N', 'txt' => 'Research Days Completed', 'comments' => '20240208'),

		array('idx' =>  101, 'originalpos' =>  660, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Umbra: Unique Resonator Slots Deployed', 'comments' => 'Dez. 2019'),
		array('idx' =>  102, 'originalpos' =>  670, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Didact: Total Fields Created', 'comments' => 'Feb. 2020'),
		array('idx' =>  103, 'originalpos' =>  680, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Didact Fields Created', 'comments' => 'Feb. 2020 und mitten im Event geändert'),
		array('idx' =>  104, 'originalpos' =>  690, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Unique Event Portals Hacked', 'comments' => '2021-04-09 - 15'),
		array('idx' =>  105, 'originalpos' =>  700, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Matryoshka Links Created', 'comments' => '2021-04-23 - 30'),
		array('idx' =>  106, 'originalpos' =>  710, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Sentinel Portals Captured', 'comments' => '2021-05-21 - 06-01'),
		array('idx' =>  107, 'originalpos' =>  720, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Event Portal Glyph Points', 'comments' => '2021-06-18 - 06-25'),
		array('idx' =>  108, 'originalpos' =>  730, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Courier AP Earned', 'badges' => array('name' => 'Courier Meet You Out There Challenge', 88888, 888888, 2888888), 'comments' => '2021-08-20 - 08-30'),
		array('idx' =>  109, 'originalpos' =>  740, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Kureze Effect', 'comments' => '2022-01-22 - 01-24'),
		array('idx' =>  110, 'originalpos' =>  750, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Comic Sans Links', 'comments' => '2022-03-31 - 04-07'),
		array('idx' =>  111, 'originalpos' =>  760, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Kythera', 'comments' => '2022-04-16..'),
		array('idx' =>  112, 'originalpos' =>  770, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'EOS Points Earned', 'comments' => '2022-05-26..2022-06-06'),
		array('idx' =>  113, 'originalpos' =>  780, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Solstice XM Recharged', 'comments' => '2022-06-09..2022-06-21'),
		array('idx' =>  114, 'originalpos' =>  790, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Superposition', 'comments' => '2022-07-29..2022-07-31'),
		array('idx' =>  115, 'originalpos' =>  800, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Peace Link Points', 'comments' => '2022-09-15..2022-09-25'),
		array('idx' =>  116, 'originalpos' =>  810, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Epiphany Dawn', 'comments' => '2022-11-18..2022-11-28'),
		array('idx' =>  117, 'originalpos' =>  820, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Echo', 'comments' => '2023-04-22..2023-06-17'),
		array('idx' =>  118, 'originalpos' =>  830, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Ctrl', 'comments' => '2023-07-15..2023-09-16'),
		array('idx' =>  119, 'originalpos' =>  850, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Discoverie', 'comments' => '2023-10-14..2023-12-16'),
		array('idx' =>  120, 'originalpos' =>  860, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Discoverie: Kinetic Capsules', 'comments' => '2023-10-15..2023-11-18'),
		array('idx' =>  121, 'originalpos' =>  870, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Discoverie: Machina Reclaims', 'comments' => '2023-10-18..2023-12-15'),
		array('idx' =>  122, 'originalpos' =>  840, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Operation Chronos Points', 'badges' => array('name' => 'Chronos', 500, 5000), 'comments' => '2023-12-06..2024-01-06'),
		array('idx' =>  123, 'originalpos' =>  880, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Cryptic Memories Global Op Points', 'badges' => array('name' => 'Cryptic Memories: Global Op', 1500, 15000), 'comments' => '2024-01-12..2024-02-05'),
		array('idx' =>  124, 'originalpos' =>  890, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Cryptic Memories', 'comments' => '2024-02-17..2024-03-16'),
		array('idx' =>  125, 'originalpos' =>  900, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Buried Memories Global Op Points', 'badges' => array('name' => 'Buried Memories Global Op', 1000, 10000), 'comments' => '2024-04-12..2024-05-06'),
		array('idx' =>  126, 'originalpos' =>  910, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Buried Memories Anomaly Points', 'comments' => '2024-04-27..2024-06-15'),
		array('idx' =>  127, 'originalpos' =>  920, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Shared Memories Global Op Points', 'badges' => array('name' => 'Shared Memories Global Op', 2000, 10000), 'comments' => '2024-07-12..2024-08-05'),
		array('idx' =>  128, 'originalpos' =>  915, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Shared Memories', 'comments' => '2024-08-17..2024-09-21'),
		array('idx' =>  129, 'originalpos' =>  921, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Field Test Dispatch', 'comments' => '2024-08-23..2024-09-02'),
		array('idx' =>  130, 'originalpos' =>  922, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Erased Memories Global Op Points', 'badges' => array('name' => 'Erased Memories Global Op', 4, 9), 'comments' => '2024-10-12..2024-10-27'),
		array('idx' =>  131, 'originalpos' =>  923, 'group' => 'EVE', 'typ' => 'N', 'txt' => 'Erased Memories', 'comments' => '2024-11-02..2024-12-14'),
		array('idx' =>  132, 'originalpos' =>  925, 'group' => 'EVE', 'typ' => 'N', 'txt' => '+Alpha Pages', 'comments' => '2025-01-18..2025-02-02'),
		array('idx' =>  133, 'originalpos' =>  926, 'group' => 'EVE', 'typ' => 'N', 'txt' => '+Alpha Op Points', 'comments' => '2025-01-18..2025-02-02'),
		array('idx' =>  134, 'originalpos' =>  924, 'group' => 'EVE', 'typ' => 'N', 'txt' => '+Alpha', 'comments' => '2025-02-15..2025-03-22'),
	);

	public $nowstattxt = array(
		array('idx' =>  0, 'originalpos' =>  10, 'typ' => 'A', 'txt' => 'Time Span'),
		array('idx' =>  1, 'originalpos' =>  20, 'typ' => 'A', 'txt' => 'Agent Name'),
		array('idx' =>  2, 'originalpos' =>  30, 'typ' => 'A', 'txt' => 'Agent Faction'),
		array('idx' =>  3, 'originalpos' =>  40, 'typ' => 'A', 'txt' => 'Date (yyyy-mm-dd)'),
		array('idx' =>  4, 'originalpos' =>  50, 'typ' => 'A', 'txt' => 'Time (hh:mm:ss)'),
		array('idx' =>  5, 'originalpos' =>  60, 'typ' => 'N', 'txt' => 'Level'),
		array('idx' =>  6, 'originalpos' =>  70, 'typ' => 'N', 'txt' => 'Lifetime AP'),
		array('idx' =>  7, 'originalpos' =>  80, 'typ' => 'N', 'txt' => 'Current AP'),
		array('idx' =>  8, 'originalpos' =>  90, 'typ' => 'N', 'txt' => 'Links Active'),
		array('idx' =>  9, 'originalpos' => 100, 'typ' => 'N', 'txt' => 'Portals Owned'),
		array('idx' => 10, 'originalpos' => 110, 'typ' => 'N', 'txt' => 'Control Fields Active'),
		array('idx' => 11, 'originalpos' => 120, 'typ' => 'N', 'txt' => 'Mind Unit Control'),
		array('idx' => 12, 'originalpos' => 130, 'typ' => 'N', 'txt' => 'Current Hackstreak'),
		array('idx' => 13, 'originalpos' => 140, 'typ' => 'N', 'txt' => 'Current Sojourner Streak'),
	);

	/**
	* Constructor
	*
	*/
	function __construct()
	{
	}

	/**
	* FindStatsIndex
	*
	* Ermittelt den Index für einen Stats-Namen
	*/
	function FindStatsIndex($text)
	{
		$key=array_search($text, array_column($this->stattxt, 'txt'));
		if($key === false) return false;
		return $this->stattxt[$key]['idx'];
	}

	/**
	* stattxtlensort
	*
	* Funktion für USort zum sortieren der Stats nach Länge, damit Teilnamen von anderen Stats am Ende geprüft werden
	*
	* USort optimiert die Sortierung (a==b and b==c also a==c) so dass subsortierungen u.U. nicht ausgeführt werden.
	* Deswegen wird der index immer in die Sortierung einbezogen. Ab PHP 8.0 könnte das hier hinfällig werden, da
	* dann die Reihenfolge bei gleichen Ergebnissen beibehalten wird
	*
	*/
	function stattxtlensort()
	{
		return function ($a, $b) {
			if(strlen($a['txt']) == strlen($b['txt'])) { // bei gleicher Textrlänge wird nach dem Index sortiert
				return $a['idx'] - $b['idx'];
			}
			return strlen($b['txt'])-strlen($a['txt']);
		};
	}

	/**
	* stattxtorgsort
	*
	* Funktion für USort zum sortieren der Stats nach der originalposition im Array
	* Wenn zwei Positionen gleich sind, dann wird nach idx sortiert
	*
	*/
	function stattxtorgsort()
	{
		return function ($a, $b) {
			if($a['originalpos'] <> $b['originalpos']) {
				return $a['originalpos']-$b['originalpos'];
			}
			return $a['idx']-$b['idx'];
		};
	}

	/**
	* stattxtidxsort
	*
	* Funktion für USort zum sortieren der Stats nach der originalposition im Array
	* Wenn zwei Positionen gleich sind, dann wird nach idx sortiert
	*
	*/
	function stattxtidxsort()
	{
		return function ($a, $b) {
			return $a['idx']-$b['idx'];
		};
	}

	/**
	* getindexlist
	*
	* Liefert ein nach idx sortiertes Array von $stattxt
	*/
	public function getindexlist()
	{
		$pos=0;
		foreach($this->stattxt AS $stat) {
			$ret[$stat['idx']]=$stat;
			$ret[$stat['idx']]['pos'] = $pos;
			$pos++;
		}
		return $ret;
	}

	/**
	* changestatsnames
	*
	* Entfernt Leerzeichen, Klammern und Doppelpunkt aus den Stats
	* Stellt ein 'J_' vorran, für decodestatstelegram, stört aber auch bei decodestatstabulator nicht
	*
	* @param string $statsname - Der zu wandelnde Name
	*/
	public function changestatsnames($statsname)
	{
		$statsname='J_'.str_replace(' ','_',$statsname);
		$statsname = str_replace("(","",$statsname);
		$statsname = str_replace(")","",$statsname);
		$statsname = str_replace("-","",$statsname);
		$statsname = str_replace(":","",$statsname);
		return $statsname;
	}

	/**
	* getAgentData
	*    Ermittelt auschließlich die Agentdaten
	*
	*
	*/
	public function getAgentData($stats)
	{
		// Next to Lines from @DhrMekmek, thanks
		$stats = trim($stats,'"'); // remove quotes, in case it's get copied from a google sheet (auto score)
		$stats = preg_replace('/ {2,}/i', ' ', $stats); // some copy/paste through other services. This handles that.
		$stats = preg_replace('/\n{2,}/i', "\n", $stats);
		// Tabs entfernen
		$stats = str_replace("\t", " ", $stats);
		if(substr($stats,0,34) <> 'Time Span Agent Name Agent Faction') {
			return false; // Keine Stats
		}
		$linestats=explode("\n",trim($stats));
		// ALL TIME janc70 Enlightened 2024-10-28 19:05:03
		// MONTH janc70 Enlightened 2024-10-28 19:42:07
		// WEEK janc70 Enlightened 2024-10-28 19:43:03
		// NOW janc70 Enlightened 2024-10-28 19:43:35
		if(preg_match('/(ALL TIME|MONTH|WEEK|NOW)\s+([a-z0-9]+)\s+(Enlightened|Resistance)\s+(\d{4}-\d{2}-\d{2})\s+(\d{2}:\d{2}:\d{2})/i', $linestats[1], $arr)) {
			$data['agentname']=$arr[2];
			$data['faction']=$arr[3];
			$data['date']=$arr[4];
			$data['time']=$arr[5];
			return $data;
		}
		return false;
	}


	/**
	* decodestats
	*    Analysiert die übergebenen Daten und ruft, wenn Stats erkannt werden die passenden Funktionen zum Dekodieren auf
	*
	* @param string $stats
	*
	* @return array Enthält die Stats, Zusetzlich Warnungen und/oder Fehlermeldungen
	*	[
	* 'info' => Telegram oder Tabulator
	* 'timezone' => Die Zeitzone, in der die Stats dekodiert wurden
	* 'timestamp' => Der Zeitstempel, der Dokodierung der Stats, nicht die Zeit in den Stats
	*  idx => [
	*   'idx' => integer - Eindeutiger Index aus $stattxt
	*   'value' => val - Der gefunden Wert
	*   'txt' => string - Name des Wertes
	*   'typ' => string - Typ des Wertes
	*   'pos' => integer - Index auf $stattxt
	*  ],...
	*  'warning' => string - Warnungen
	*  'warningarr' => array - [
	*    'typ' => string - Warnungstyp
	*    'txt' => string (unbekannter Statsstring)
	*    'after' => string (nach diesen Stats)
	*    'before' => string (vor diesen Stats)
	*  ]
	*  'error' => string - Fehlermeldungen, dann ausschließlich dieser und error_code-Eintrag
	*  'error_code' => integer - Fehlercode
	*    1 - Keine Stats
	*    2 - Stats nich auswertbar
	*    3 - Nicht die ALL TIME Stats
	* ]
	*/
	public function decodestats($stats)
	{
//		$stats=trim($stats);
		// Next to Lines from @DhrMekmek, thanks
		$stats = trim($stats,'"'); // remove quotes, in case it's get copied from a google sheet (auto score)
		$stats = preg_replace('/ {2,}/i', ' ', $stats); // some copy/paste through other services. This handles that.
		$stats = preg_replace('/\n{2,}/i', "\n", $stats);
		if(substr($stats,0,34) == 'Time Span Agent Name Agent Faction') {
			return $this->decodestatstelegram($stats); // Ohne Tabulatoren, die werden von Telegram gelöscht
		}
		if(substr($stats,0,34) == "Time Span\tAgent Name\tAgent Faction") {
			return $this->decodestatstabulator($stats); // Mit Tabulatoren, aus einer anderen Quelle als Telegram
		}
//		error_log("Falsche Stats");
		return array('error' => 'Das sind keine Stats', 'error_code' => 1);
	}

	/**
	* decodestatstabulator
	*    Aufruf für Stats mit Tabulator-Trennung, Original-Ausgabe von Prime
	*
	* @param string $stats
	*
	* @return array Enthält die Stats, Zusetzlich Warnungen und/oder Fehlermeldungen
	*
	*
	*/
	public function decodestatstabulator($stats)
	{
		$retarr=array('info' =>'tabulator', 'timezone' => date_default_timezone_get(), 'timestamp' => time());
		$linestats=explode("\n",trim($stats));
		$names=explode("\t",$linestats[0]); // Wertbezeichner als Array
		$values=explode("\t",$linestats[1]); // Werte als Array

		if(count($values) < $this->minimumstatsnumber) {
			$retarr['warning'].="⚠ Nicht genügend Stats-Werte\n";
			$retarr['warningarr'][]=array('typ' => 'Not enough stats values', 'txt' => 'Not enough stats values');
		}
		if($values[0] <> 'ALL TIME') {
			return array('error' => 'Nicht die Gesamt-Stats', 'error_code' => 3);
		}

		foreach($this->stattxt AS $statpos => $stat) { // Array nach Namen erzeugen
			if(IsSet($namestat[$this->changestatsnames($stat['txt'])])) { // Doppelter eintrag (2021-03-19 'Kinetic Capsules Completed')
				$idx=1;
				while(IsSet($namestat[$this->changestatsnames($stat['txt'])."_$idx"])) {
					$idx++;
				}
				$namestat[$this->changestatsnames($stat['txt'])."_$idx"] = $statpos;
			}else{
				$namestat[$this->changestatsnames($stat['txt'])] = $statpos;
			}
		}
//		error_log(json_encode($namestat));

		$foundnames=array(); // Nimmt die gefundenen Namen auf um doppelte zu erkennen
//		$linestats=trim($linestats[0]); // Statistiknamen als zusammenhängender String

		$valuepos=0;
		$nextunbekannteintrag=true;
		while(IsSet($names[$valuepos])) {
			$namesvaluepos=$this->changestatsnames($names[$valuepos]);

			if(IsSet($foundnames[$namesvaluepos])) { // Doppelter Name, Index $idx anhängen und hochzählen
				$idx=1;
				while(IsSet($foundnames[$namesvaluepos."_$idx"])) {
					$idx++;
				}
				$namesvaluepos=$namesvaluepos."_$idx";
			}

			$foundnames[$namesvaluepos]=true; // In foundnames schreiben, falls es nochmal kommt
			if(IsSet($namestat[$namesvaluepos])) {
				$arraypos=$namestat[$namesvaluepos];
				$arraypostxt=$this->stattxt[$arraypos]['txt'];
				if(IsSet($this->stattxt[$arraypos]['alt'])) $arraypostxt = $this->stattxt[$arraypos]['alt'];
				$retarr[$this->stattxt[$arraypos]['idx']]=array('idx' => $this->stattxt[$arraypos]['idx'], 'value' => $values[$valuepos], 'txt' => $arraypostxt, 'typ' => $this->stattxt[$arraypos]['typ'], 'pos' => $arraypos);
			}elseif(trim($names[$valuepos]) <> '') {
				// Unbekannter Eintrag
				if(!IsSet($retarr['warning'])) $retarr['warning']=''; else $retarr['warning'].="\n";
				$retarr['warning'].="Unbekannter Eintrag: ".$names[$valuepos]." nach ".$retarr[$this->stattxt[$arraypos]['idx']]['txt'];
				$retarr['warningarr'][]=array('typ' => 'unknown name', 'txt' => $names[$valuepos], 'after' => $retarr[$this->stattxt[$arraypos]['idx']]['txt']);
			}
			$valuepos++;
		}
		return $retarr;
	}

	/**
	* decodestatstelegram
	*    Aufruf für Stats ohne Tabulator-Trennung, Telegram löscht Tabulatoren und ersetzt sie durch Leerzeichen
	*
	* @param string $stats
	*
	* @return array Enthält die Stats, Zusetzlich Warnungen und/oder Fehlermeldungen
	*
	*
	*/
	public function decodestatstelegram($stats)
	{
		/*
		/* Nach dem Caos, das NIA kurz vor dem FS am 3.4.2021 durch umbenennen und umsortieren der Stats verursacht hat, arbeitet die Routine nach einer Idee und Beispiel von @DhrMekmek
		/* */

		$stattxt=$this->stattxt;
		usort($stattxt,$this->stattxtlensort()); // Sortier das Array nach der Länge der Statsnamen
//		error_log(json_encode($this->stattxt)."\n");

		$retarr=array('info' =>'telegram', 'timezone' => date_default_timezone_get(), 'timestamp' => time());
		$linestats=explode("\n",trim($stats));
		$values=explode(" ",$linestats[1]); // Werte als Array
		$linestats=trim($linestats[0]); // Statistiknamen als zusammenhängender String

		// Testen ob Datum an der richtigen Stelle ("GESAMT" kann in anderen Sprachen aus mehreren Wörtern bestehen)
		// jetzt ALL TIME, da aber auch tägliche, wöchentliche und Monatliche Stats existieren, bleibt das so
		$i=0;
		while(IsSet($values[3+$i]) and $values[3+$i]<>date('Y-m-d',strtotime($values[3+$i])) and $i < 6) $i++;
		if(!IsSet($values[3+$i]) or $values[3+$i]<>date('Y-m-d',strtotime($values[3+$i]))) {
			// Kein korrektes Datum gefunden
			return array('error' => "Konnte Stats nicht auswerten", 'error_code' => 2);
		}
		for ($j=1; $j<=$i; $j++) { // Datum war nicht an der richtigen Stelle jetzt entsprechend die ersten Einträge des Array zusammen führen
			$values[0]=$values[0]." ".$values[$j];
			unset($values[$j]);
		}
		if($i>0) $values=array_values($values); // Array neu Indizieren
		// Ende: Testen ob Datum an der richtigen Stelle

		if(count($values) < $this->minimumstatsnumber) {
			$retarr['warning'].="⚠ Nicht genügend Stats-Werte\n";
			$retarr['warningarr'][]=array('typ' => 'Not enough stats values', 'txt' => 'Not enough stats values');
		}

		if($values[0] <> 'ALL TIME') {
			return array('error' => 'Nicht die Gesamt-Stats', 'error_code' => 3);
		}

		// Ersetzen der Statnamen durch Einfachwörter mit Offset J_
		foreach($stattxt AS $statindex => $statdata) {
			$ersatztext=$statdata['txt']; // Der Ersatztext
			if(IsSet($statdata['alt'])) $ersatztext=$statdata['alt']; // oder, wenn ein alt-Attribut existiert
			$ersatztext=$this->changestatsnames($ersatztext); // Leerzeichen, Klammern und Doppelpunkt entfernen
			$pattern=$statdata['txt'];
			$pattern=str_replace('(', '\(', $pattern); // Klammern schützen für regex
			$pattern=str_replace(')', '\)', $pattern); // Klammern schützen für regex
			$pattern=str_replace('+', '\+', $pattern); // Plus schützen für regex
			// preg_replace statt str_replace, weil ich die Ersetzungen begrenzen kann
			// Leerzeichen oder Zeilenbeginn am Anfang um doppelte Ersetzungen zu vermeiden, führendes Leerzeichen wird hinterher mit trim entfernt
			if($linestats=preg_replace('/(?>^|\s)('.$pattern.')/', ' '.$ersatztext, $linestats, 1)) {
				$linenames[$ersatztext] = $statindex;
			}
			$linestats=trim($linestats);
		}

		// zerlegen der Stats
		$valuenames=explode(" ", $linestats);

		// Die Prüfung auf unterschiedliche Anzahl Namen - Werte als Indiz für neue Werte entfernt, da es bei einteiligen Namen nicht anschlägt (2202-04-18 Kythera)

		// Prüfen auf neue Stats
		$i=0;
		while(IsSet($valuenames[$i])) {
			if(substr($valuenames[$i], 0,2) <> 'J_') { // Ein nicht ersetzter Name
				while(IsSet($valuenames[$i+1]) and substr($valuenames[$i+1], 0,2) <> 'J_') { // und es folgen nicht ersetze Namen
					$valuenames[$i].=" ".$valuenames[$i+1];
					unset($valuenames[$i+1]);
					$valuenames=array_values($valuenames); // Neu indizieren
				}
				if(!IsSet($retarr['warning'])) $retarr['warning']=''; else $retarr['warning'].="\n";
				$after=$stattxt[$linenames[$valuenames[$i-1]]]['txt'];
				$afteropos=$stattxt[$linenames[$valuenames[$i-1]]]['originalpos'];
				$before='END';
				$beforeopos='9999';
				if(IsSet($linenames[$valuenames[$i+1]])) {
					$before=$stattxt[$linenames[$valuenames[$i+1]]]['txt'];
					$beforeopos=$stattxt[$linenames[$valuenames[$i+1]]]['originalpos'];
				}
				$retarr['warning'].="⚠ Unbekannter Eintrag: <code>".$valuenames[$i]."</code>\nnach <code>$after / $afteropos</code> und\nvor <code>$before / $beforeopos</code>\n";
				$retarr['warningarr'][]=array('typ' => 'unknown name', 'txt' => $valuenames[$i], 'after' => $after, 'afteropos' => $afteropos, 'before' => $before, 'beforeopos' => $beforeopos);
				$i++;
			}else{
				$i++;
			}
		}
		if(count($valuenames) <> count($values)) { // Nochmal prüfen, oben Namen und Werte übereinstimmen
			$retarr['warning'].="Anzahl Namen zu Werten unterschiedlich\n"; // Nur Warnung, Kein Fehler, da die Werte bis zum unbekannten Namen stimmen sollten
			$retarr['warningarr'][]=array('typ' => 'namevaluedifference');
		}

		$valuepos=0;
		$last=array('name' => '', 'originalpos' => 0); // Array zum merken des letzten Eintrags zum prüfen der originalpos
		while(IsSet($valuenames[$valuepos])) {
			if(IsSet($linenames[$valuenames[$valuepos]])) { // Ein bekannter Eintrag
				$arraypos=$linenames[$valuenames[$valuepos]];
				$realvaluename=$stattxt[$arraypos]['txt'];
				if(IsSet($stattxt[$arraypos]['alt'])) $realvaluename = $stattxt[$arraypos]['alt'];
				$retarr[$stattxt[$arraypos]['idx']]=array('idx' => $stattxt[$arraypos]['idx'], 'value' => $values[$valuepos], 'txt' => $realvaluename, 'typ' => $stattxt[$arraypos]['typ'], 'pos' => $arraypos);
				if($this->checkoriginalpos == true) { // Prüfen, ob die originalpos im $stattxt array passt
					if($stattxt[$arraypos]['originalpos'] < $last['originalpos']) {
						// Positionsangaben in $stattxt wiedersprechen der gefundenen Position
						if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'warning',"Originalreihenfolge in den Stats differiert:\n".$last['name']." / ".$last['originalpos']."\nvor ".$stattxt[$arraypos]['txt']." / ".$stattxt[$arraypos]['originalpos'],'main');
					}
					$last['name'] = $stattxt[$arraypos]['txt'];
					$last['originalpos'] = $stattxt[$arraypos]['originalpos'];
				}
			}
			$valuepos++;
		}
		return $retarr;
	}

	/**
	* createTSV
	*   Generiert aus dem von decodestats erzeugten Array Tabulator Seperated Stats, wie Ingress selber
	*   Natürlich werden nur die Stats enthalten sein, die das Script kennt
	*
	* @param array $stats - Array mit Stats entsprechend stattxt
	* @param array $filter - Kann ein Array mit idx enthalten, dann werden nur die in die Ausgabe übernommen
	*/
	public function createTSV($stats, $filter=null)
	{
		$stattxt=$this->stattxt;
		usort($stattxt,$this->stattxtorgsort()); // Sortier das Array nach der originalposition
//		error_log(json_encode($this->stattxt));

		$statnamestr='';
		$statvaluestr='';
		foreach($stattxt AS $stattxtdata) { // Das sortierte $stattxt durchlaufen
			$index=$stattxtdata['idx'];
			if(!is_array($filter) or IsSet($filter[$index])) { // Wenn das Array Filter gesetzt ist, dann nur Stats verwenden, die in Filter vorhanden sind
				if(IsSet($stats[$index])) {
					$statnamestr.=$stattxtdata['txt']."\t";
					$statvaluestr.=$stats[$index]['value']."\t";
				}
			}
		}
		return $statnamestr."\n".$statvaluestr;
	}

	/**
	* statArrayPHPOut
	*
	* Erzeugt aus dem $stattxt-Array einen PHP-Quellcode-Text des Arrays
	*
	**/
	public function statArrayPHPOut($stattxt)
	{
		$code='public $stattxt = array('."\n";
		foreach($stattxt AS $stats) {
			$code.="\tarray('idx' => ".str_pad($stats['idx'], 4, ' ', STR_PAD_LEFT).", 'originalpos' => ".str_pad($stats['originalpos'], 4, ' ', STR_PAD_LEFT).", 'group' => '".$stats['group']."', 'typ' => '".$stats['typ']."', 'txt' => '".$stats['txt']."'";
			if(IsSet($stats['alt'])) { // Alternativer Stats-Name (z.B.: Kinetic Capsules Completed)
				$code.=", 'alt' => '".$stats['alt']."'";
			}
			if(IsSet($stats['badges'])) { // Diese Stats enthalten Badges
				$code.=", 'badges' => array(";
				$first=true;
				foreach($stats['badges'] AS $bidx => $badges) {
					if($first == true) {
						$first=false;
					}else{
						$code.=", ";
					}
					if(!is_integer($bidx)) {
						$code.="'".$bidx."' => ";
					}
					if(is_integer($badges)) {
						$code.=$badges;
					}else{
						$code.="'".$badges."'";
					}
				}
				$code.=")";
			}
			if(IsSet($stats['comments'])) { // Kommentare, waren vorher als Kommentar hinter dem Array-Eintrag
				$code.=", 'comments' => '".$stats['comments']."'";
			}
			$code.="),\n";
		}
		$code.=");\n";
		return $code;
/*
	public $stattxt = array(
		array('idx' =>  0, 'originalpos' =>  10, 'typ' => 'A', 'txt' => 'Time Span'),
		array('idx' => 108, 'originalpos' => 544, 'typ' => 'N', 'txt' => 'Courier AP Earned', 'badges' => array('name' => 'Courier Meet You Out There Challenge', 88888, 888888, 2888888)), // 2021-08-20 - 08-30
		array('idx' => 55, 'originalpos' => 540, 'typ' => 'N', 'txt' => 'Kinetic Capsules Completed', 'alt' => 'Event Kinetic Capsules Completed', 'comments' =>'2021-03-19 Doppelter Eintrag für Event, extra Zähler für den Zeitraum'),
*/
	}

	/**
	* getNewOriginalposNumbers
	*
	* Sortiert das Array $stattxt nach der Originalposition und vergibt diese danach neu mit einem Abstand von 10
	*
	**/
	public function getNewOriginalposNumbers()
	{
		$stattxt=$this->stattxt;
		usort($stattxt,$this->stattxtorgsort()); // Sortier das Array nach der originalposition
		$pos=10;
		foreach($stattxt AS &$stats) {
			$stats['originalpos']=$pos;
			$pos=$pos+10;
		}
		usort($stattxt, $this->stattxtidxsort()); // Sortiert das Array wieder nach dem Index
		return $this->statArrayPHPOut($stattxt);
	}
}
?>