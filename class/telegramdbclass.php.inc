<?php

/**** Klasse für Telegram in Verbindung mit einer DB
*
*	@Author: Jan Clauß
* @Version: 1.1 (2021-04-15)
* 
* Required Class telegram
*/

include_once "telegramclass.php.inc";

class telegramdb extends telegram
{
	
	public $pdo = null; // Das PDO-object für den Bot (Constructor)
	public $database_table_telegram = ''; // Die Datenbank für den Bot (Constructor)
	public $updateuserdata = false; // Aktualisiere die Userdaten in der DB selbstständig
	public $default_timezone = 'Europe/Berlin';
	public $default_language = 'ger';
	public $default_botconf = '[]';
	public $kommandovalid = 10*60; // Gültigkeit eines Kommandos, also wie lange wird auf eine Antwort gewartet
	
	public $rightsarray=array('mod' => 10,'user' => 8,'waiting' => 6,'ignore' => 4, 'block' => 2);
	
	// Bot-Konfiguration in DB-Satz 0 // Die Daten sollten erst bei Nutzung geladen werden und auch gleich wieder zurück geschrieben werden, da Instanzen des Bot parallel laufen
	public $masterbotconf=null;
	public $masterbotconf_hash='';
	
	// Werden von decodemessage - get_userdata gefüllt
	public $user_id=0; // User-ID aus letzter Nachricht
	public $username='';
	public $first_name='';
	public $last_name='';
	public $language_code='';
	public $rights='block';
	public $rightslevel=0;
	public $language='ger';
	public $timezone='Europe/Berlin';
	public $command='';
	public $botconf='';
	public $lastkontakt=0;
	
	public $chat_defaultrights='waiting';
	public $chat_id=0; // User-ID des Chats, bei Gruppen unterschiedlich zur User-id (user_id Sender-ID, Chat_ID ist die ID der Gruppe
	public $chat_title='';
	public $chat_rights='waiting';
	public $chat_rightslevel=0;
	public $chat_type='';
	public $chat_usedb=false; // Legt für den Chat einen extra Datensatz an
	public $chat_language='ger';
	public $chat_timezone='Europe/Berlin';
	public $chat_botconf=array();
	public $chat_lastkontakt=0;
	
	public $cancelstring='/cancel';
	public $cancelstringausgabe='canceled';
	public $cancelthenexit=false; // Nach dem Abbrechen des Kommandos wird die Script-Ausführung beendet
	
	function __construct($bot_token, $pdoobj, $database_table)
	{
		parent::__construct($bot_token);
		if(!is_object($pdoobj))
			return(array('ok' => false, 'error_code' => '1', 'description' => '$pdo must be an Database Object'));
		$this->pdo=$pdoobj;
		$this->database_table_telegram=$database_table;
		
		return(array('ok' => true));
  }
  
	/*
	* readmasterbotconf
	*
	* Liest, wenn aktiviert die Masterbotconf aus DB mit user_id=0
	* Erstellt eine Prüfsumme über die gelesene Botconf
	
	public $masterbotconf=null;
	public $masterbotconf_hash='';

	*/
	public function readmasterbotconf()
	{
  	$sql="SELECT botconfjson FROM ".$this->database_table_telegram." WHERE user_id=0";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(false);
		}else{
			$this->masterbotconf=array();
			if($stm_all->rowCount() == 1) {
				$result=$stm_all->fetch();
				$masterbotconf=json_decode($result['botconfjson'],true);
				if(is_array($masterbotconf)) $this->masterbotconf=$masterbotconf;
				$this->masterbotconf_hash=sha1(json_encode($this->masterbotconf));
			}else{
				// Datensatz 0 anlegen
				$sql="INSERT INTO ".$this->database_table_telegram." SET user_id=0, first_name='Bot-Master-DS', commandjson='', botconfjson=''";
				$stm_all=$this->pdo->prepare($sql);
				$stm_all->execute();
				if($stm_all->errorInfo()[0]<>'00000') {
					error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
					if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
					return(false);
				}
			}
		}
	}
	
	/*
	* writemasterbotconf
	*
	* Schreibt, wenn aktiviert und sich etwas geändert hat die Masterbotconf in DB mit user_id=0
	* Zur Prüfung auf die Veränderung wird der Hash benutzt
	*/
	public function writemasterbotconf()
	{
		if(($this->masterbotconf_hash == '' and count($this->masterbotconf) > 0) or // Erste Daten 
		   ($this->masterbotconf_hash <> sha1(json_encode($this->masterbotconf)))) { // Daten haben sich geändert
			$sql="UPDATE ".$this->database_table_telegram." SET botconfjson=:botconfjson, lastkontakt=:time WHERE user_id=0";
			$stm_all=$this->pdo->prepare($sql);
			$masterbotconf_json=json_encode($this->masterbotconf);
			$time=time();
			$stm_all->bindParam(':botconfjson',$masterbotconf_json, PDO::PARAM_STR);
			$stm_all->bindParam(':time',$time, PDO::PARAM_INT);
			$stm_all->execute();
			if($stm_all->errorInfo()[0]<>'00000') {
				error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
				if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
				return(false);
			}else{
				$this->masterbotconf_hash=sha1(json_encode($this->masterbotconf)); // Hash aktualisieren
			}
		}
	}
	
  function createdb($defaultrights='waiting')
  {
  	$db=explode('.',$this->database_table_telegram);
  	$database=$db[0];
  	$table=$db[1];
  	$sql="USE $database";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(json_encode(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo())));
		}
  	$sql="
CREATE TABLE IF NOT EXISTS `".$table."` (
	`user_id` BIGINT(20) NOT NULL,
	`user_name` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8mb4_bin',
	`first_name` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8mb4_bin',
	`last_name` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8mb4_bin',
	`rights` ENUM('mod','user','waiting','ignore','block') NOT NULL DEFAULT '".$defaultrights."' COLLATE 'utf8mb4_bin',
	`language` CHAR(10) NOT NULL DEFAULT 'ger' COLLATE 'utf8mb4_bin',
	`timezone` VARCHAR(255) NOT NULL DEFAULT 'Europe/Berlin' COLLATE 'utf8mb4_bin',
	`commandjson` TEXT(65535) NOT NULL COLLATE 'utf8mb4_bin',
	`botconfjson` TEXT(65535) NOT NULL COLLATE 'utf8mb4_bin',
	`lastkontakt` INT(11) NOT NULL DEFAULT '0',
	`grouptools` ENUM('Y','N') NOT NULL DEFAULT 'Y' COMMENT 'Zum Gruppieren bei Abfragen' COLLATE 'utf8mb4_bin',
	UNIQUE INDEX `user_id` (`user_id`) USING BTREE
)
COLLATE='utf8mb4_bin'
ENGINE=InnoDB
;
";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(json_encode(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo())));
		}
		return(array('ok' => true, 'db' => $database, 'table' => $table));
  }
  
  //************************** 2020-12-14
  //* ALTER TABLE `".$table."`	
	//* CHANGE COLUMN `user_id` `user_id` BIGINT NOT NULL DEFAULT 0 FIRST;
	//**************************
  
  private function get_userdata($user_arr,$autoinsert=false,$recursion=false)
  // Nur für den aktuellen User, die Werte des Users werden in die Klasse eingetragen, 
  // der User wird ggfls angelegt (wenn Kommando /start ist, oder wenn autoinsert=true)
  // $recursion wrd nur intern genutzt
  // Werden Daten eines Nutzers gebraucht, verwende getuserds()
  {
  	$user_id=$user_arr['user_id'];
  	$this->user_id=$user_id; //Selbst wenn User nicht in DB, die User_id ist bekannt
  	$this->language_code=$user_arr['language_code']; // Wird nicht in der DB gespeichert, dafür ist language da.
		// Chat-Daten
		$this->chat_id=$user_arr['chat']['id'];
		if(IsSet($user_arr['chat']['title'])) $this->chat_title=$user_arr['chat']['title'];
		$this->chat_type=$user_arr['chat']['type'];
  	$commandjson='[]';
  	$lastkontakt=time();
  	$sql="SELECT user_name, first_name, last_name, rights, language, timezone, commandjson, botconfjson, lastkontakt FROM ".$this->database_table_telegram." WHERE user_id=:user_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':user_id',$user_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			$ret=array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo());
		}else{
			if($stm_all->rowCount() <> 1) {
				// User nicht in DB gefunden
				if(($user_arr['command'] == '/start' or $autoinsert == true) and $recursion == false) { // Nur eine Recursion erlaubt
					//User meldet sich am Bot an, Datenbankeintrag erstellen
					$sql="INSERT INTO ".$this->database_table_telegram." SET user_id=:user_id, user_name=:user_name, first_name=:first_name, last_name=:last_name, commandjson='[]', botconfjson=:botconfjson, lastkontakt=:lastkontakt, timezone=:timezone, language=:language";
					$stm_all=$this->pdo->prepare($sql);
					$stm_all->bindParam(':user_id',$user_id, PDO::PARAM_INT);
					$stm_all->bindParam(':user_name',$user_arr['username'], PDO::PARAM_STR);
					$stm_all->bindParam(':first_name',$user_arr['first_name'], PDO::PARAM_STR);
					$stm_all->bindParam(':last_name',$user_arr['last_name'], PDO::PARAM_STR);
					$stm_all->bindParam(':botconfjson',$this->default_botconf, PDO::PARAM_STR);
					$stm_all->bindParam(':lastkontakt',$lastkontakt, PDO::PARAM_INT);
					$stm_all->bindParam(':timezone',$this->default_timezone, PDO::PARAM_STR);
					$stm_all->bindParam(':language',$this->default_language, PDO::PARAM_STR);
					$stm_all->execute();
					if($stm_all->errorInfo()[0]<>'00000') {
						error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
						if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
						$ret=array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo());
					}else{
						$ret=$this->get_userdata($user_arr,$autoinsert,true); // Einfach selber aufrufen, um die Daten in $ret zu laden
					}
				}else{
					$ret=array('ok' => false, 'error_code' => '1', 'description' => 'User not found');
				}
			}else{
				// User in DB
				$ret['ok'] = true;
				$ret['user_id'] = $user_id;
				$this->user_id = $user_id;
				$retarr=$stm_all->fetch(PDO::FETCH_ASSOC);
				$ret['username'] = $retarr['user_name'];
				$this->username = $retarr['user_name'];
				$ret['first_name'] = $retarr['first_name'];
				$this->first_name = $retarr['first_name'];
				$ret['last_name'] = $retarr['last_name'];
				$this->last_name = $retarr['last_name'];
				$ret['rights'] = $retarr['rights'];
				$this->rights = $retarr['rights'];
				$ret['rightslevel']=$this->rightsarray[$retarr['rights']];
				$this->rightslevel=$this->rightsarray[$retarr['rights']];
				$ret['language'] = $retarr['language'];
				$this->language = $retarr['language'];
				$ret['timezone'] = $retarr['timezone'];
				$this->timezone = $retarr['timezone'];
				$ret['command'] = json_decode($retarr['commandjson'],true);
				$this->command = json_decode($retarr['commandjson'],true);
				$ret['botconf'] = json_decode($retarr['botconfjson'],true);
				$this->botconf = json_decode($retarr['botconfjson'],true);
				$ret['lastkontakt'] = $retarr['lastkontakt'];
				$this->lastkontakt = $retarr['lastkontakt'];
				// Daten in DB aktualisieren?
				if($this->updateuserdata and ($this->username <> $user_arr['username'] or $this->first_name <> $user_arr['first_name'] or $this->last_name <> $user_arr['last_name'])) {
					$sql="UPDATE ".$this->database_table_telegram." SET user_name=:user_name, first_name=:first_name, last_name=:last_name WHERE user_id=:user_id";
					$stm_all=$this->pdo->prepare($sql);
					$stm_all->bindParam(':user_name',$user_arr['username'], PDO::PARAM_STR);
					$stm_all->bindParam(':first_name',$user_arr['first_name'], PDO::PARAM_STR);
					$stm_all->bindParam(':last_name',$user_arr['last_name'], PDO::PARAM_STR);
					$stm_all->bindParam(':user_id',$user_id, PDO::PARAM_INT);
					$stm_all->execute();
					if($stm_all->errorInfo()[0]<>'00000') {
						error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
						if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
					}
				}
				// Lastkontakt aktualisieren
				$sql="UPDATE ".$this->database_table_telegram." SET lastkontakt=:lastkontakt WHERE user_id=:user_id";
				$stm_all=$this->pdo->prepare($sql);
				$stm_all->bindParam(':user_id',$user_id, PDO::PARAM_INT);
				$stm_all->bindParam(':lastkontakt',$lastkontakt, PDO::PARAM_INT);
				$stm_all->execute();
				if($stm_all->errorInfo()[0]<>'00000') {
					error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
					if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
				}
			}
			if($user_arr['chat']['id'] <> $user_id) { // Kein privater Chat
				$chat_id=$user_arr['chat']['id'];
				if(($user_arr['chat']['type'] == 'group' or $user_arr['chat']['type'] == 'supergroup') and $this->chat_usedb==true) {  // Erst mal nur bei Gruppen, anderes noch testen
					// Chat-Daten Abfragen
  				$sql="SELECT user_name, first_name, rights, language, timezone, botconfjson, lastkontakt FROM ".$this->database_table_telegram." WHERE user_id=:chat_id";
					$stm_all=$this->pdo->prepare($sql);
					$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
					$stm_all->execute();
					if($stm_all->errorInfo()[0]<>'00000') {
						error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
						if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
						$ret=array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo());
					}else{
						if($stm_all->rowCount() <> 1) {
							// Chat anlegen
							$sql="INSERT INTO ".$this->database_table_telegram." SET user_id=:chat_id, user_name=:chat_type, first_name=:chat_title, rights=:chat_rights, timezone=:timezone, language=:language, botconfjson=:botconfjson, lastkontakt=:lastkontakt, commandjson=''";
							$stm_all=$this->pdo->prepare($sql);
							$stm_all->bindParam(':chat_id',$this->chat_id, PDO::PARAM_INT);
							$stm_all->bindParam(':chat_type',$this->chat_type);
							$stm_all->bindParam(':chat_title',$this->chat_title, PDO::PARAM_STR);
							$stm_all->bindParam(':chat_rights',$this->chat_defaultrights, PDO::PARAM_STR);
							$chat_botconfjson=json_encode($this->chat_botconf);
							$stm_all->bindParam(':botconfjson',$chat_botconfjson, PDO::PARAM_STR); // Es ist eh noch nix in der botconf
							$stm_all->bindParam(':timezone',$this->chat_timezone, PDO::PARAM_STR);
							$stm_all->bindParam(':language',$this->chat_language, PDO::PARAM_STR);
							$stm_all->bindParam(':lastkontakt',$lastkontakt,PDO::PARAM_INT);
							$stm_all->execute();
							if($stm_all->errorInfo()[0]<>'00000') {
								error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
								if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
								$ret=array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo());
							}
							$this->chat_rights=$this->chat_defaultrights;
							$this->chat_rightslevel=$this->rightsarray[$this->chat_rights];
							$this->chat_lastkontakt=$lastkontakt;
						}else{
							// Chat-Daten laden
							$retarr=$stm_all->fetch(PDO::FETCH_ASSOC);
							if($retarr['first_name'] <> $this->chat_title or $retarr['user_name'] <> $this->chat_type) {
								$sql="UPDATE ".$this->database_table_telegram." SET first_name=:chat_title, user_name=:chat_type WHERE user_id=:chat_id";
								$stm_all=$this->pdo->prepare($sql);
								$stm_all->bindParam(':chat_id',$this->chat_id, PDO::PARAM_INT);
								$stm_all->bindParam(':chat_title',$this->chat_title, PDO::PARAM_STR);
								$stm_all->bindParam(':chat_type',$this->chat_type);
								$stm_all->execute();
								if($stm_all->errorInfo()[0]<>'00000') {
									error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
									if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
								}
							}
							$this->chat_rights=$retarr['rights'];
							$this->chat_rightslevel=$this->rightsarray[$this->chat_rights];
							$this->chat_language=$retarr['language'];
							$this->chat_timezone=$retarr['timezone'];
							$this->chat_botconf=json_decode($retarr['botconfjson'],true);
							if(!is_array($this->chat_botconf)) $this->chat_botconf=array();
							$this->chat_lastkontakt=$retarr['lastkontakt'];
							// lastkontakt aktualisieren
							$sql="UPDATE ".$this->database_table_telegram." SET lastkontakt=:lastkontakt WHERE user_id=:chat_id";
							$stm_all=$this->pdo->prepare($sql);
							$stm_all->bindParam(':chat_id',$this->chat_id, PDO::PARAM_INT);
							$stm_all->bindParam(':lastkontakt',$lastkontakt, PDO::PARAM_INT);
							$stm_all->execute();
							if($stm_all->errorInfo()[0]<>'00000') {
								error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
								if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
							}
						}
					}
				}
			}
		}
		return($ret);
  }

	public function decodemessage($message,$autoinsert=false)	
	// if $autoinsert == true, wird der Nutzer automatisch angelegt, ohne /start
	{
		$ret=parent::decodemessage($message);
		if(IsSet($ret['message_typ'])) {
			$dbdata=$this->get_userdata($ret,$autoinsert);
			$ret['db']=$dbdata;
			if(!IsSet($ret['chat']['type']) or $ret['chat']['type'] == 'private') { // Damit ein im Privaten ausgeführtes Kommando keinen Chat behindert
				if(IsSet($ret['db']['command']['command']) and $ret['db']['command']['command'] <> '' and IsSet($ret['text']) and strtolower(substr(trim($ret['text']),0,strlen($this->cancelstring))) == strtolower($this->cancelstring)) {
					// Internes Kommando abbrechen
					$this->setcommand($this->user_id,'');
					if(IsSet($ret['db']['command']['command'])) unset($ret['db']['command']['command']);
					if($this->cancelstringausgabe <> '') $this->sendmessage($this->user_id,$this->cancelstringausgabe);
					if($this->cancelthenexit == true) exit();
				}
				if(IsSet($ret['db']['command']['command']) and $ret['db']['command']['command'] <> '' and $ret['db']['command']['commandtime'] > time() - $this->kommandovalid) { // Kommandos älter als kommandvalid s werden ignoriert
					$ret['message_typ']='command';
					if(count($ret['db']['command']['args'])>0) { // Argumente in Array args übergeben
						$ret['command']=$ret['db']['command']['command'];
						$ret['args']=$ret['db']['command']['args'];
					}elseif(preg_match('/^(\/?[a-z0-9]+)(.*)/i',$ret['db']['command']['command'],$arr)) { // Argumente mit Kommando übergeben oder keine Argumente
						$ret['command']=$arr[1];
						$ret['args']=array();
						if(trim($arr[2]) <> '') {
							preg_match_all('/([^ ]+)/',$arr[2], $args);
							$ret['args']=$args[0];
						}
					}
				}
			}
		}
		return($ret);
	}
	
	public function MigrateFromChatId($old, $new)
	{
		$sql="UPDATE ".$this->database_table_telegram." SET user_id=:chat_id WHERE user_id=:from_chat_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$new, PDO::PARAM_INT);
		$stm_all->bindParam(':from_chat_id',$old, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
		}
		parent::MigrateFromChatId($old, $new);
	}	
	
	public function setcommand($user_id, $command, $args=array())
	{
		if($user_id == 0) $user_id=$this->user_id;
		$arr['command']=$command;
		$arr['commandtime']=time();
		$arr['args']=$args;
		$json=json_encode($arr);
		$sql="UPDATE ".$this->database_table_telegram." SET commandjson=:commandjson WHERE user_id=:user_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':user_id',$user_id, PDO::PARAM_INT);
		$stm_all->bindParam(':commandjson',$json, PDO::PARAM_STR);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			$ret=array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo());
		}else{
			$ret=array('ok' => true);
		}
	}	
	
	public function writebotconf($user_id=0, $botconf='-')
	{
		if($botconf == '-') $botconf=$this->botconf;
		if($user_id == 0) $user_id=$this->user_id;
		if(!is_array($botconf)) {
			error_log(__FILE__.', Line '.__LINE__.' Botconf kein Array'); // Schutz vor dem Fehlerhaften überschreiben der Botconf
		}else{
			$json=json_encode($botconf);
			$sql="UPDATE ".$this->database_table_telegram." SET botconfjson=:botconf WHERE user_id=:user_id";
			$stm_all=$this->pdo->prepare($sql);
			$stm_all->bindParam(':user_id',$user_id, PDO::PARAM_INT);
			$stm_all->bindParam(':botconf',$json, PDO::PARAM_STR);
			$stm_all->execute();
			if($stm_all->errorInfo()[0]<>'00000') {
				error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
				if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
				$ret=array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo());
			}else{
				$ret=array('ok' => true);
			}
		}
	}

	public function chat_readbotconf($chat_id=0)
	{
		$sql="SELECT botconfjson FROM ".$this->database_table_telegram." WHERE user_id=:chat_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			$ret=array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo());
		}else{
			if($stm_all->rowCount() == 1) {
				$result=$stm_all->fetch();
				$this->chat_botconf=json_decode($result['botconfjson'],true);
			}
		}
	}

	public function chat_writebotconf($chat_id=0, $botconf='-')
	{
		if($botconf == '-') $botconf=$this->chat_botconf;
		if($chat_id == 0) $chat_id=$this->chat_id;
		if(!is_array($botconf)) {
			error_log(__FILE__.', Line '.__LINE__.' Botconf kein Array'); // Schutz vor dem Fehlerhaften überschreiben der Botconf
		}else{
			$json=json_encode($botconf);
			$sql="UPDATE ".$this->database_table_telegram." SET botconfjson=:botconf WHERE user_id=:chat_id";
			$stm_all=$this->pdo->prepare($sql);
			$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
			$stm_all->bindParam(':botconf',$json, PDO::PARAM_STR);
			$stm_all->execute();
			if($stm_all->errorInfo()[0]<>'00000') {
				error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
				if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
				$ret=array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo());
			}else{
				$ret=array('ok' => true);
			}
		}
	}

	public function settimezone($user_id, $timezone)
	{
		if(!date_default_timezone_set($timezone))	{
			return(array('ok' => false, 'error_code' => '1', 'description' => 'Timezone Error', 'errorInfo' => 'false Timezone'));
		}
		$sql="UPDATE ".$this->database_table_telegram." SET timezone=:timezone WHERE user_id=:user_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':user_id',$user_id, PDO::PARAM_INT);
		$stm_all->bindParam(':timezone',$timezone, PDO::PARAM_STR);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}else{
			return(array('ok' => true));
		}
	}
	
	public function setlanguage($user_id, $language)
	{
		if(!preg_match('/^([a-z0-9]+)$/i', strtolower($language), $arr)) {
			return(array('ok' => false, 'error_code' => '1', 'description' => 'Language Error', 'errorInfo' => 'Invalid characters'));
		}else{
			$sql="UPDATE ".$this->database_table_telegram." SET language=:language WHERE user_id=:user_id";
			$stm_all=$this->pdo->prepare($sql);
			$stm_all->bindParam(':user_id',$user_id, PDO::PARAM_INT);
			$stm_all->bindParam(':language',$arr[1], PDO::PARAM_STR);
			$stm_all->execute();
			if($stm_all->errorInfo()[0]<>'00000') {
				error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
				if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
				return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
			}else{
				return(array('ok' => true));
			}
		}
	}
	
	public function deleteuser($user_id)
	{
		$sql="DELETE FROM ".$this->database_table_telegram." WHERE user_id=:user_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':user_id',$user_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}else{
			return(array('ok' => true));
		}
	}
	
	public function getuserrights($user_id)
	{
		$sql="SELECT user_name, first_name, last_name, rights FROM ".$this->database_table_telegram." WHERE user_id=:user_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':user_id',$user_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}else{
			if($stm_all->rowCount() <> 1) {
				return(array('ok' => false, 'error_code' => '3', 'description' => 'User not found','errorInfo' => 'User not found'));
			}
			$resarr=$stm_all->fetch(PDO::FETCH_ASSOC);
			return(array('ok' => true, 'user_name' => $resarr['user_name'], 'first_name' => $resarr['first_name'], 'last_name' => $resarr['last_name'], 'rights' => $resarr['rights']));
		}
	}
	
	public function getuseridfromname($username) {
		// Username muss mit @ übergeben werden!
		if(preg_match('/^@([a-z][a-z0-9_]+)$/i',$username,$arr)) {
			$sql="SELECT user_id FROM ".$this->database_table_telegram." WHERE user_name=:arr1";
			$stm_all=$this->pdo->prepare($sql);
			$stm_all->bindParam(':arr1',$arr[1], PDO::PARAM_STR);
		}elseif(preg_match('/^([0-9]+)$/',$username,$arr)) {
			$sql="SELECT user_id FROM ".$this->database_table_telegram." WHERE user_id=:arr1";
			$stm_all=$this->pdo->prepare($sql);
			$stm_all->bindParam(':arr1',$arr[1], PDO::PARAM_INT);
		}else{
			return(array('ok' => false, 'error_code' => '2', 'description' => 'No Username or id'));
		}
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}else{
			if($stm_all->rowCount() == 1) {
				$resarr=$stm_all->fetch(PDO::FETCH_ASSOC);
				return(array('ok' => true, 'user_id' => $resarr['user_id']));
			}else{
				return(array('ok' => false, 'error_code' => '1', 'description' => 'User not found'));
			}
		}
	}

	public function getuserds($user_id)
	// Liefert den DB-Datensatz des Nutzers, ['botconfjson'] kommt zusätzlich decodiert in ['botconf']
	{
  	$sql="SELECT * FROM ".$this->database_table_telegram." WHERE user_id=:user_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':user_id',$user_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}else{
			if($stm_all->rowCount() <> 1) {
				return(array('ok' => false, 'error_code' => '3', 'description' => 'User not found','errorInfo' => 'User not found'));
			}else{
				$resarr=$stm_all->fetch(PDO::FETCH_ASSOC);
				$resarr['botconf']=json_decode($resarr['botconfjson'],true);
				$resarr['ok']=true;
				return($resarr);
			}
		}
	}
	
	public function getmods()
	{
		$sql="SELECT user_id FROM ".$this->database_table_telegram." WHERE rights='mod' and user_id<>0";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'Error','errorInfo' => $stm_all->errorInfo()));
		}
		while($resarr=$stm_all->fetch(PDO::FETCH_ASSOC)) {
			$ret['mods'][]=$resarr['user_id'];
		}
		$ret['ok']=true;
		return($ret);
	}
	
	public function setuserrights($user_id, $rights,$degradieren=false)
	
	// # $user_id - id des zu ändernden Users
	// # $rights - die neuen Rechte
	// # $degradieren - bei true, kann man sich selber degradieren
	{
		$rights=strtolower($rights);
		$old=$this->getuserrights($user_id); // Bestehende Rechte abrufen
		if($old['ok'] == false) { 
			return(array('ok' => false, 'error_code' => '3', 'description' => 'User not found','errorInfo' => 'User not found'));
		}
		if(!$degradieren and $user_id == $this->user_id and $this->rightsarray[$rights] < $this->rightsarray[$this->rights]) {
			return(array('ok' => false, 'error_code' => '4', 'description' => 'Your self','errorInfo' => 'You can not set your own rights'));
		}
		if($old['rights'] == $rights) { // Nix zu ändern
			return($old);
		}
		$sql="UPDATE ".$this->database_table_telegram." SET rights=:rights WHERE user_id=:user_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':user_id',$user_id, PDO::PARAM_INT);
		$stm_all->bindParam(':rights',$rights, PDO::PARAM_STR);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		$old['rights']=$rights;  // Neue Rechte ins Array für die Rückgabe
		return($old);
	}
	
	public function removeolduser($tage,$exclude='0')
	//*
	//* $tage, User die länge als $tage nicht am Bot waren werden gelöscht
	//* $exclude, Kommagetrennte Liste ($string) von Usern, die nicht gelöscht werden sollen
	//*
	{
//		global $fs; //!Debug
		$tagetstmp=time()-$tage*24*60*60; // Alle Timestamps die älter sind löschen
		if(!preg_match('/^[0-9,]+$/',$exclude)) { // Parameter $exclude prüfen
			return(array('ok' => false, 'error_code' => '2', 'description' => 'Parameter Fehler','errorInfo' => 'exclude enthält ungültige Liste'));
		}
		$sql="DELETE FROM ".$this->database_table_telegram." WHERE user_id > 0 and lastkontakt<:tagetstmp and rights <> 'mod' and user_id NOT IN ($exclude)";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':tagetstmp',$tagetstmp, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		return(array('ok' => true, 'rowCount' => $stm_all->rowCount()));
	}
	
	//*****************************************
	// Chats
	//*****************************************
	
	public function getchats($chattyp='', $testechats=false)
	// ezeugt eine Liste aller Chats
	// $chattyp = group | supergroup - kann einschränken
	// $testechats - Prüft, ob der Bot noch in den Chats ist und löscht sie, wenn nicht
	{
		$sql="SELECT * FROM ".$this->database_table_telegram." WHERE user_id < 0";
		if($chattyp == 'group' or $chattyp == 'supergroup') $sql=' AND user_name=:chattyp';
		$stm_all=$this->pdo->prepare($sql);
		if($chattyp == 'group' or $chattyp == 'supergroup') $stm_all->bind_param(':chattyp', $chattyp, PDO::PARAM_STR);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		if($stm_all->rowCount() == 0) {
			// Kein Chat gefunden
			return(array('ok' => true));
		}
		while($resarr=$stm_all->fetch()) {
			if($testechats == true and $this->testechat($resarr['user_id']) == false) {
				$this->deleteuser($resarr['user_id']); // Chat aus der Liste löschen
			}else{
				$retarr[]=array('chat_id' => $resarr['user_id'], 'title' => $resarr['first_name'], 'type' => $resarr['user_name']);
			}
		}
		$ret=array('ok' => true);
		if(IsSet($retarr)) $ret['result']=$retarr;
		return($ret);
	}
	
	public function getchatname($chat_id)
	{
		if($chat_id >= 0) return(false);
		$sql="select first_name FROM ".$this->database_table_telegram." WHERE user_id=$chat_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		if($stm_all->rowCount() <> 1) return(false);
		$resarr=$stm_all->fetch();
		return($resarr['first_name']);
	}
	
	public function getchatlanguage($chat_id)
	{
		if($chat_id >= 0) return(false);
		$sql="select language FROM ".$this->database_table_telegram." WHERE user_id=$chat_id";
		$stm_all=$this->pdo->prepare($sql);
		$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
		$stm_all->execute();
		if($stm_all->errorInfo()[0]<>'00000') {
			error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
			if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
			return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
		}
		if($stm_all->rowCount() <> 1) return(false);
		$resarr=$stm_all->fetch();
		return($resarr['language']);
	}
	
	public function setchatlanguage($chat_id, $language)
	{
		if(!preg_match('/^([a-z0-9]+)$/i', strtolower($language), $arr)) {
			return(array('ok' => false, 'error_code' => '1', 'description' => 'Language Error', 'errorInfo' => 'Invalid characters'));
		}else{
			$sql="UPDATE ".$this->database_table_telegram." SET language=:language WHERE user_id=:chat_id";
			$stm_all=$this->pdo->prepare($sql);
			$stm_all->bindParam(':chat_id',$chat_id, PDO::PARAM_INT);
			$stm_all->bindParam(':language',$arr[1], PDO::PARAM_STR);
			$stm_all->execute();
			if($stm_all->errorInfo()[0]<>'00000') {
				error_log(__FILE__.' '. __LINE__.' '.print_r($stm_all->errorInfo(),true));
				if(function_exists('logthis')) logthis(__FILE__, __LINE__, 'sql-error',$stm_all->errorInfo(),'main');
				return(array('ok' => false, 'error_code' => '2', 'description' => 'SQL-Error','errorInfo' => $stm_all->errorInfo()));
			}else{
				return(array('ok' => true));
			}
		}
	}
}

?>