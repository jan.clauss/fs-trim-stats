<?php

class Autoloader {
	static public function loader($className) {
		$filename = 'class/' . str_replace('\\', '/', $className) . 'class.php.inc';
		if (file_exists($filename)) {
			include($filename);
			if (class_exists($className)) {
				return TRUE;
			}
		}
		return FALSE;
	}
}
spl_autoload_register('Autoloader::loader');

?>