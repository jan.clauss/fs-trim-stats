<?php
//Ü
$statsarray[ 0]=array('idx' => 1, 'name' => 'Agentname', 'human' => 'Agentname', 'einheit' => '', 'fevgames' => '');
$statsarray[ 1]=array('idx' => 2, 'name' => 'Faction', 'human' => 'Faction', 'einheit' => '', 'fevgames' => '');
$statsarray[ 2]=array('idx' => 3, 'name' => 'Date', 'human' => 'Date', 'einheit' => '', 'fevgames' => '');
$statsarray[ 3]=array('idx' => 4, 'name' => 'Time', 'human' => 'Time', 'einheit' => '', 'fevgames' => '');
$statsarray[ 4]=array('idx' => 5, 'name' => 'Level', 'human' => 'Level', 'einheit' => '', 'fevgames' => 'Level');
$statsarray[ 5]=array('idx' => 6, 'name' => 'Lifetime AP', 'human' => 'Lebenszeit AP', 'einheit' => 'AP', 'fevgames' => 'Lifetime AP');
$statsarray[ 6]=array('idx' => 7, 'name' => 'AP', 'human' => 'aktuelle AP', 'einheit' => 'AP', 'fevgames' => 'Current AP');
$statsarray[ 7]=array('idx' => 11, 'name' => 'XM Collected', 'human' => 'XM Collected', 'einheit' => 'XM', 'fevgames' => 'XM Collected');
$statsarray[ 8]=array('idx' => 13, 'name' => 'Distance Walked', 'human' => 'Distance Walked', 'einheit' => 'km', 'fevgames' => 'Distance Walked');
$statsarray[ 9]=array('idx' => 14, 'name' => 'Resonators Deployed', 'human' => 'Resonators Deployed', 'einheit' => '', 'fevgames' => 'Resonators Deployed');
$statsarray[10]=array('idx' => 15, 'name' => 'Links Created', 'human' => 'Links Created', 'einheit' => '', 'fevgames' => 'Links Created');
$statsarray[11]=array('idx' => 16, 'name' => 'Control Fields Created', 'human' => 'Control Fields Created', 'einheit' => '', 'fevgames' => 'Control Fields Created');
$statsarray[12]=array('idx' => 20, 'name' => 'XM Recharged', 'human' => 'XM Recharged', 'einheit' => 'XM', 'fevgames' => 'XM Recharged');
$statsarray[13]=array('idx' => 21, 'name' => 'Portals Captured', 'human' => 'Portals Captured', 'einheit' => '', 'fevgames' => 'Portals Captured');
$statsarray[14]=array('idx' => 23, 'name' => 'Mods Deployed', 'human' => 'Mods Deployed', 'einheit' => '', 'fevgames' => 'Mods Deployed');
$statsarray[15]=array('idx' => 24, 'name' => 'Resonators Destroyed', 'human' => 'Resonators Destroyed', 'einheit' => '', 'fevgames' => 'Resonators Destroyed');
$statsarray[16]=array('idx' => 25, 'name' => 'Portals Neutralized', 'human' => 'Portals Neutralized', 'einheit' => '', 'fevgames' => 'Portals Neutralized');
$statsarray[17]=array('idx' => 34, 'name' => 'Hacks', 'human' => 'Hacks', 'einheit' => '', 'fevgames' => 'Hacks');
$statsarray[18]=array('idx' => 35, 'name' => 'Glyph Hack Points', 'human' => 'Glyph Hack Points', 'einheit' => '', 'fevgames' => '');
$statsarray[19]=array('idx' => 8, 'name' => 'Unique Portals Visited', 'human' => 'Unique Portals Visited', 'einheit' => '', 'fevgames' => 'Unique Portals Visited');
$statsarray[20]=array('idx' => 22, 'name' => 'Unique Portals Captured', 'human' => 'Unique Portals Captured', 'einheit' => '', 'fevgames' => 'Unique Portals Captured');

$usestats=array(0,1,4,5,12); // Diese Daten werden ausgegeben
?>