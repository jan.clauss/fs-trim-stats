<?php
	$answer=gettext('
Sende Deine Ingress Stats an diesen Bot:
  Der Bot extrahiert die relevanten Stats für den FS aus Deinen Gesamt-Stats und formatiert sie für das AutoScore-Sheet.
  
Der Bot speichert Deine Stats nicht, benutzt Du ihn "wie er ist" legt er auch keinen Datensatz für Dich an. Erst wenn Du Einstellungen vornimmst, die er sich merken muss, legt er einen Datensatz in der DB für Dich an. Was darin steht erfährst Du mit dem Kommando /dsgvo

Befehle:
	/help - Diese Hilfe
	/config - Für Deine individuelle Konfiguration, legt einen Datensatz für Dich an
	/stats - Zeigt die eingestellten Stats an
	/botinfo - Infos zum bot
	/clear - Löscht Deine Daten aus der Bot-DB
	/dsgvo - Gibt Dir einen Auszug Deiner Daten der DB
');
	$tg->sendmessage($tg->user_id,$answer);
	exit;
?>