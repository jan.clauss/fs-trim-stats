<?php
	$tgres=$tg->decodemessage($msg,true); // Lädt die Userdaten und legt den User bei Bedarf an
	if($tgres['db']['ok'] == false) {
		$tg->sendmessage($tg-user_id,gettext('Fehler beim lesen/anlegen der Userdaten'));
		error_log(json_encode($tgres));
		exit;
	}
	if(IsSet($tgres['db']['createds']) and $tgres['db']['createds'] == true) {
		$tg->sendmessage($tg->user_id, gettext('Datensatz angelegt'));
		$tg->botconf['usestats']=$usestats;
		$tg->writebotconf();
	}
//	$tg->sendmessage($tg->user_id,json_encode($tgres));
	if(IsSet($tgres['args'][0])) { // Unterfunktion übergeben
		switch($tgres['args'][0]) {
			case 'none':
				exit;
			break;
			case 'stats':
				$answer='select:';
				if(!IsSet($tgres['args'][2])) { // Auswahl anbieten
					$editstats=$tgres['args'][1];
					foreach($statsarray AS $idx => $stats) {
						$statsname=$stats['name'];
						if(IsSet($tg->botconf['usestats'][$editstats]) and $tg->botconf['usestats'][$editstats] == $idx) $statsname='✅ '.$statsname;
						$configarray[]=array(array('text' => $statsname, 'callback_data' => "config stats $editstats $idx"));
					}
					$configarray[]=array(array('text' => gettext('Zurück'), 'callback_data' => "config"));
					if($editstats <> 'neu') {
						$configarray[]=array(array('text' => '🗑 '.gettext('Löschen'), 'callback_data' => "config stats $editstats del"));
					}
				}else{ // Auswahl / Aktion eintragen/ ausführen
					if($tgres['args'][2] == 'del' and IsSet($tg->botconf['usestats'][$tgres['args'][1]])) { // stats löschen
						array_splice($tg->botconf['usestats'], $tgres['args'][1], 1);
//						unset($tg->botconf['usestats'][$tgres['args'][1]]);
					}else{
						if($tgres['args'][1] == 'neu') {
							$tg->botconf['usestats'][]=intval($tgres['args'][2]);
						}else{
							$tg->botconf['usestats'][intval($tgres['args'][1])]=intval($tgres['args'][2]);
						}
					}
					$tg->writebotconf();
					unset($tgres['args'][0]);
				}
			break;
		}
	}
	if(!IsSet($tgres['args'][0])) {
		// Wertungen für diesen FS
		foreach($tg->botconf['usestats'] AS $pos => $wert) {
			if(IsSet($statsarray[$wert]['name'])) {
				$textoffset='';
				if($wert<7) $textoffset='❗';
				if($wert<4) $textoffset='‼';
				$configarray[]=array(array('text' => $textoffset.' '.$statsarray[$wert]['name'], 'callback_data' => "config stats $pos"));
			}else{ // unbekannter Stats-Eintrag
				$configarray[]=array(array('text' => '❗ ???', 'callback_data' => "config stats $pos"));
			}
		}
		$configarray[]=array(array('text' => '➕ '.gettext('Neu'), 'callback_data' => "config stats neu"));
		$configarray[]=array(array('text' => gettext('Abbruch'), 'callback_data' => 'cancel'));
	}
	if(IsSet($configarray)) {
		$configarray=array('inline_keyboard'=>$configarray);

		if(!IsSet($answer)) {
			$answer=gettext('Wähle die Stats');
		}
		$answer=array(
			'text' => $answer,
			'reply_markup' => json_encode($configarray)
		);
		if($tgres['message_typ']=='callback_query') {
			$answer['message_id']=$tgres['message_id'];
		}
	}
	$tg->sendmessage($tg->user_id,$answer);
	exit;
?>