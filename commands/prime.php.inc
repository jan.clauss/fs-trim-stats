<?php

	$writeno=false; // Bei Fehler true, dann kein Schreiben der Stats
  // Prime Stats auswerten
  $answer='';
  $infoanswer=''; // Warnungen, Zeitfenster, User
  $primestat = new primestats();
  $stats=$primestat->decodestats($tgres['text']);
  // Details anzeigen?
  if(IsSet($tg->botconf['primetest']) and $tg->botconf['primetest'] > time()) {
  	$answer='';
  	foreach($stats AS $idx => $wert) {
  		if(is_int($idx)) {
	  		$answer.=$wert['txt'].": ".$wert['value']."\n";
	  	}
  	}
//  	$answer=json_encode($stats,true);
  	$tg->sendmessage($tg->user_id, $answer);
  	$answer='';
  	// Zurücksetzen
		$tg->botconf['primetest']=0;
		$tg->writebotconf();
  }
  
  // Fehler und Warnungen aus decodestats
  if(IsSet($stats['warning'])) {
  	$infoanswer.="⚠ ".$stats['warning']."\n";
  	$infoanswer.=gettext("Überprüfe die Werte")."\n";
  }
  
  // Stats ausgeben
  $answer="Manual\n";
  if(IsSet($tg->botconf['usestats'])) $usestats=$tg->botconf['usestats']; // Es gibt individuelle Stats im User-DS
  foreach($usestats AS $stat) {
  	$answer.=$stats[$statsarray[$stat]['idx']]['value']."\n";
  }
  $tg->sendmessage($tg->user_id, $answer);
  exit;
?>