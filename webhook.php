<?php
//Ü	
	if (php_sapi_name() != 'cli') { // Im Kommandozeilenmodus braucht es kein flush ;-)
		header( 'Content-type: text/html; charset=utf-8' ); // https://www.php.net/manual/de/function.ob-flush.php
		header("HTTP/1.0 200 OK"); // Wenn das zu lange dauert, sendet Telegram sonst die Anfrage erneut.
		flush();
		ob_flush();
	}
	
	include_once "class/Autoloaderclass.php.inc";

	include_once "/var/www/hiddenvariables.FStrimstatbot.php.inc";
	include "inc/statsarray.php.inc";

	// Datenbankklasse Standard PDO-Objekt
	$pdo=connectpdo();
	if(!is_object($pdo)) {
		error_log("PDO-Error 1");
		die ("PDO-Error 1");
	}
	// Fehlerbehandlung auf Silent (Standard bis PHP8, dann ist ERRMODE_EXCEPTION standard)
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
	
	// Telegram-Klasse telegramfsclass - telegramdbclass - telegramclass
	$tg=new telegramdb($bot_token,$pdo,$database_table_main);
	if(!is_object($tg)) {
		error_log("Telegram-Error 2");
		die ("Telegram-Error 2");
	}

	// Sorgt dafür, dass der Bot Nachrichten nicht ändert, sondern löscht und neu erstellt
	$tg->delete_not_editmessage=true;
	
	if(IsSet($argv[1]) and $argv[1]=='cron') { // Cron-Aufruf
		
		exit;	
	}elseif(IsSet($_GET['info'])) {
		echo $bot_info;
	}elseif(IsSet($_GET['instwebhook'])) {
		$res=$tg->installwebhook();
		echo $res;
		exit;
	}elseif(IsSet($_GET['uninstwebhook'])) {
		$res=$tg->uninstallwebhook();
		echo $res;
		exit;
	}elseif(IsSet($_GET['sendmsg'])) { // Zum Testen
		$res=$tg->sendmessage($bot_owner,"Telegram Bot Test");
		echo $res;
		exit;
	}elseif(IsSet($_GET['createdb'])) { // Anlegend der Datenbank
		include 'webhook.createdb.php.inc';
		exit;
	}else{
		// *******
		// Ab hier der eigentliche BOT
		// *******
		$msg=file_get_contents("php://input");
		$msgarr=json_decode($msg,true);
		$tgres=$tg->decodemessage($msg,false); // User wird in DB nicht automatisch angelegt!
		if(!IsSet($tgres['message_typ'])) { // War das überhaupt was von Telegram?
			echo "undefinierter Aufruf";
			// Fehler bei der Dekodierung
		}else{
			if(IsSet($tg->botconf['statconfig'])) {
				// Individuelle Stats aus der DB nutzen
			}
			// Zeitzone anpassen
			date_default_timezone_set($tg->timezone);
		}
		if(IsSet($tgres['text']) and substr($tgres['text'],0,15)=='Time Span Agent') {
			// Stats gesendet
			include "commands/prime.php.inc";
			exit();
		}
		switch($tgres['message_typ']) {
			// ******************** Query *******************************
			case 'query':
				switch($tgres['command']) {
					case '/help':
						// Hilfetext
						include "commands/help.php.inc";
					break;
					case '/botinfo':
						// Bot-Informationen
						include "commands/botinfo.php.inc";
					break;
					case '/dsgvo':
						// Info über gespeicherte Daten
						include "commands/dsgvo.php.inc";
					break;
					case '/config':
						// Einstellungen des Stats, der Sprache...
						include "commands/config.php.inc";
					break;
					case '/clear':
						// löscht alle Daten des NUtzers aus der DB
						include "commands/clear.php.inc";
					break;
					case '/stats':
						// Zeigt die aktuellen Stats an
						include "commands/stats.php.inc";
					break;
				}
			break;
			// ******************* Callback *****************************
			case 'callback_query':
				switch($tgres['command']) {
					case 'config':
						include "commands/config.php.inc";
					break;
				}
			break;
			// *************** Command ****************************
			case 'command': // Kommandos (DB) die auf Antwort warten
				switch($tgres['command']) {
					case 'config':
						include "commands/config.php.inc";
					break;
				}
			break;
			// ************** Location ****************************
			case 'location': // Übersenden der Position
			break;
			// ************** Document ****************************
			case 'document': // Übersenden einer Datei
			break;
		}
		// Jede Funktion beendet das Script selber, daher kommt die folgende Ausgabe nur, wenn keine gültige Funktion benutzt wurde
		$answer=gettext("Sende Deine Stats an den Bot und erhalte die Kurzform für das AutoScoreSheet");
		$tg->sendmessage($tg->user_id,$answer);
	}

?>